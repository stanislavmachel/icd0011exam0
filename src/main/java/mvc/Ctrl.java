package mvc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class Ctrl {

    @GetMapping("/")
    public String root() {
        return "root";
    }

    // kood tuleb siia ...


    @PostMapping("/transform")
    public List<String> post(@RequestBody List<Entry> listOfEntries) {

        HashSet<String> result = new LinkedHashSet<>();

        List<Entry> collect = listOfEntries.stream().filter(entry -> Objects.nonNull(entry.getCount()) && entry.getCount() > 1).collect(Collectors.toList());

        collect.forEach(entry -> {
            result.add(entry.getDate());
        });

        return new ArrayList<>(result);
    }

}