package mvc;

import lombok.Data;

@Data
public class Entry {
    private String date;
    private Integer count;
}
