package security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

//        http.exceptionHandling()
//                .accessDeniedHandler((request, response, accessDeniedException) -> {
//                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//                });

//        http.exceptionHandling().authenticationEntryPoint((request, response, authException) -> {
//            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//        });
//
//        http.logout().logoutSuccessHandler((request, response, authentication) -> {
//            response.setStatus(HttpServletResponse.SC_OK);
//        });

        http.csrf().disable();
        var sf = new SecurityFilter(authenticationManager(), "/login");

        http.authorizeRequests().antMatchers("/login/**").permitAll();
        http.authorizeRequests().antMatchers("**").authenticated();

        http.addFilterBefore(sf, LogoutFilter.class);

    }

    @SuppressWarnings("deprecation")
    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.inMemoryAuthentication()
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .withUser("alice")
                .password("s3cr3t")
                .roles("USER");
    }

}